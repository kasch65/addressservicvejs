# Linux commands:

## Install NPM - @root
sudo apt update  
sudo apt install nodejs  
sudo apt install npm


## Initially clone and configure app
cd /opt  
sudo adduser addressservice  
sudo -u addressservice -H git clone https://gitlab.com/kasch65/addressservicvejs.git  
#sudo chown addressservice: addressservicvejs

## Build and start - User: @addressservice
cd /opt/addressservicvejs  
npm run deploy:prod  
... stop after success

## Configure Service - @root
sudo nano /etc/systemd/system/addressservice.service

    [Unit]
    Description=Address REST Service
    After=network.target

    [Service]
    Type=simple
    Environment=CI=true

    User=addressservice
    Group=springboot

    # Wait for DB
    # ExecStartPre=/opt/waitForPort.sh 3306

    # The configuration file application.properties should be here:
    #change this to your workspace
    WorkingDirectory=/opt/addressservicvejs

    #path to executable.
    #executable is a bash script which calls jar file
    ExecStart=/usr/bin/nodejs /usr/bin/npm run deploy:prod

    TimeoutStartSec=300
    SuccessExitStatus=143
    TimeoutStopSec=10
    Restart=on-failure
    RestartSec=15

    [Install]
    WantedBy=multi-user.target

sudo systemctl daemon-reload
sudo systemctl start addressservice
systemctl status addressservice

## Open Firewall
sudo ufw allow proto tcp from any to 159.69.120.154 port 3002

# Open at this URL
Demo GUI: http://addressservice.fairweben.de:3002/
REST API: http://addressservice.fairweben.de:3002/list (open with HTTP POST)