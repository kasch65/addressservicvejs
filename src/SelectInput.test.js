import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import SelectInput from './SelectInput';

let container = null;
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it('renders without crashing', () => {
    let option = null;
    let selectedVar = false;
    act(() => {
        render(<SelectInput
            title="Zip"
            opts={['28195', '28203']}
            value="28203"
            state="valid"
            selectHandler={
                (value, selected) => {
                    console.log('1.: value, selected: ', value, selected);
                    option = value;
                    selectedVar = selected;
                }
            } />
            , container);
    });

    // Component rendered with correct values?
    expect(container.querySelector('label').textContent).toBe('Zip');
    expect(container.querySelector('input').value).toBe('28203');
    expect(container.querySelector('input').classList.item(0)).toBe('valid');
    expect(container.querySelector('select').size).toBe(2);
    expect(container.querySelector('select').options.item(0).textContent).toBe('28195');

    // Test handler for option select called?
    const option1 = container.querySelector('select').options.item(0);
    option1.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    //expect(option).toBe('28195');
    expect(selectedVar).toBe(true);

    // Test handler for input called?
    const input1 = container.querySelector('input');
    expect(input1).not.toBeNull();
    //input1.dispatchEvent(new KeyboardEvent('keypress', { bubbles: true, key: "x", char: "x" }));
    input1.dispatchEvent(new KeyboardEvent('input', { bubbles: true, key: "x", char: "x", data: "x" }));
    expect(option).toBe('28203'); // Input is ignored
    expect(selectedVar).toBe(false);
});
