
require('dotenv').config()
var mysql = require('mysql')
var pool = mysql.createPool({
	connectionLimit : process.env.DB_CONNECTIONS,
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_DATABASE
})

const zipCnt = filter => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE zip LIKE '${filter}%'` : ''
		const sql = `SELECT COUNT(*) FROM zip ${filterClause}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			const result = Object.values(results[0])[0]
			return resolve(result)
		})
	})
}

const zipList = (filter, pagination) => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE zip LIKE '${filter}%'` : ''
		const sql = `SELECT zip FROM zip ${filterClause} ORDER BY zip LIMIT ${pagination.pageSize * (pagination.page - 1)}, ${pagination.pageSize}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			return resolve(results.map(i => i.zip))
		})
	})
}

const cityCnt = filter => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE cityname LIKE '${filter}%'` : ''
		const sql = `SELECT COUNT(*) FROM cities ${filterClause}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			const result = Object.values(results[0])[0]
			return resolve(result)
		})
	})
}

const cityList = (filter, pagination) => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE cityname LIKE '${filter}%'` : ''
		const sql = `SELECT * FROM cities ${filterClause} ORDER BY cityname, id LIMIT ${pagination.pageSize * (pagination.page - 1)}, ${pagination.pageSize}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			return resolve(results.map(i => { return { id: i.id, name: i.cityname, adminLevel: i.adminlevel } }))
		})
	})
}

const streetCnt = filter => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE streetname LIKE '${filter}%'` : ''
		const sql = `SELECT COUNT(*) FROM streets ${filterClause}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			const result = Object.values(results[0])[0]
			return resolve(result)
		})
	})
}

const streetList = (filter, pagination) => {
	return new Promise((resolve, reject) => {
		const filterClause = filter ? ` WHERE streetname LIKE '${filter}%'` : ''
		const sql = `SELECT * FROM streets ${filterClause} ORDER BY streetname, highway, id LIMIT ${pagination.pageSize * (pagination.page - 1)}, ${pagination.pageSize}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			return resolve(results.map(i => { return { id: i.id, name: i.streetname, relevance: i.highway } }))
		})
	})
}

const addressCnt = filter => {
	return new Promise((resolve, reject) => {
		const zipMatcher = filter && filter.zip && filter.zip.zip ? ` AND z.zip LIKE '${filter.zip.zip}%'` : ''
		const cityMatcher = filter && filter.city && filter.city.name ? ` AND c.cityname LIKE '${filter.city.name}%'` : ''
		const streetMatcher = filter && filter.street && filter.street.name ? ` AND s.streetname LIKE '${filter.street.name}%'` : ''
		const filterClause = zipMatcher || cityMatcher || streetMatcher ? ` WHERE 1${zipMatcher}${cityMatcher}${streetMatcher}` : ''
		const sql = `SELECT COUNT(*)
			FROM
				addresses a
				INNER JOIN zip z
					ON z.id = a.zipid
				INNER JOIN cities c
					ON c.id = a.cityid
				INNER JOIN streets s
					ON s.id = a.streetid
			${filterClause}
			GROUP BY
				z.zip,
				c.cityname,
				s.streetName`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			const result = Object.values(results[0])[0]
			return resolve(result)
		})
	})
}

const addressList = (filter, pagination) => {
	return new Promise((resolve, reject) => {
		const zipMatcher = filter && filter.zip && filter.zip.zip ? ` AND z.zip LIKE '${filter.zip.zip}%'` : ''
		const cityMatcher = filter && filter.city && filter.city.name ? ` AND c.cityname LIKE '${filter.city.name}%'` : ''
		const streetMatcher = filter && filter.street && filter.street.name ? ` AND s.streetname LIKE '${filter.street.name}%'` : ''
		const filterClause = zipMatcher || cityMatcher || streetMatcher ? ` WHERE 1${zipMatcher}${cityMatcher}${streetMatcher}` : ''
		const sql = `SELECT
				GROUP_CONCAT(DISTINCT z.id) zipid,
				z.zip,
				GROUP_CONCAT(DISTINCT c.id) cityid,
				c.cityname,
				GROUP_CONCAT(DISTINCT c.adminlevel) adminlevel,
				GROUP_CONCAT(DISTINCT s.id) streetid,
				s.streetname,
				GROUP_CONCAT(DISTINCT s.highway) relevance
			FROM
				addresses a
				INNER JOIN zip z
					ON z.id = a.zipid
				INNER JOIN cities c
					ON c.id = a.cityid
				INNER JOIN streets s
					ON s.id = a.streetid
			${filterClause}
			GROUP BY
				z.zip,
				c.cityname,
				s.streetName
			ORDER BY
				z.zip,
				c.cityname,
				s.streetName
			LIMIT ${pagination.pageSize * (pagination.page - 1)}, ${pagination.pageSize}`
		pool.query(sql, (err, results) => {
			if (err) {
				return reject(err)
			}
			return resolve(results.map(i => {
				const zip = {id: i.zipid, zip: i.zip}
				const city = {id: i.cityid, name: i.cityname, adminLevel: i.adminlevel}
				const street = {id: i.streetid, name: i.streetname, relevance: i.relevance}
				return { zip, city, street }
			}))
		})
	})
}

module.exports = { zipCnt, zipList, cityCnt, cityList, streetCnt, streetList, addressCnt, addressList }