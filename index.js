require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const compression = require('compression')
const bodyParser = require('body-parser')
const cors = require('cors')
const adressService = require('./adressService')

const _getPgination = (req) => {
	let pageSize = req.query.pageSize
	if (!pageSize) {
		pageSize = 100
	}
	else {
		pageSize = Number(pageSize)
	}
	let page = req.query.page
	if (!page) {
		page = 1
	}
	else {
		page = Number(page)
	}
	return {pageSize, page}
}

console.log('All inits done.')
console.log('Starting server...')

// The REST service:
const app = express()

// compress all responses
app.use(compression())

//app.use(morgan('tiny'))
app.use(morgan((tokens, req, res) => {
	return [
		tokens.method(req, res),
		tokens.url(req, res),
		tokens.status(req, res),
		tokens.res(req, res, 'content-length'), '-',
		tokens['response-time'](req, res), 'ms',
		JSON.stringify(req.body)
	].join(' ')
}))

app.use(cors())
app.use(express.static('build'))
app.use(bodyParser.json())

// URLs =======================================================================
app.get('/cnt/zip', (req, res, next) => {
	adressService.zipCnt(req.query.filter)
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.get('/list/zip', (req, res, next) => {
	adressService.zipList(req.query.filter, _getPgination(req))
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.get('/cnt/streets', (req, res, next) => {
	adressService.streetCnt(req.query.filter)
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.get('/list/streets', (req, res, next) => {
	adressService.streetList(req.query.filter, _getPgination(req))
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.get('/cnt/cities', (req, res, next) => {
	adressService.cityCnt(req.query.filter)
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.get('/list/cities', (req, res, next) => {
	adressService.cityList(req.query.filter, _getPgination(req))
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.post('/cnt', (req, res, next) => {
	adressService.addressCnt(req.body)
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

app.post('/list', (req, res, next) => {
	adressService.addressList(req.body, _getPgination(req))
		.then(result => res.json(result))
		.catch(err => next(err.sqlMessage))
})

// Auf ungültige URLs reagieren
const unknownEndpoint = (request, response) => response.status(404).send({ error: 'unknown endpoint' })
app.use(unknownEndpoint)

// Fehler an client senden
const errorHandler = (error, request, response, next) => {
	console.error('Error: ', error.message)

	if (error.name === 'CastError' && error.kind === 'ObjectId') {
		return response.status(400).send({ error: 'malformatted id' })
	}
	next(error)
}
app.use(errorHandler)

// Get port from environment
const PORT = process.env.PORT || 3002
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`)
})
